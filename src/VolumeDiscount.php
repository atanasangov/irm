<?php

namespace IRM;


class VolumeDiscount implements DiscountInterface
{
    /** @var int */
    private $quantity;
    /** @var float */
    private $discount;

    /**
     * VolumeDiscount constructor.
     *
     * @param int   $quantity
     * @param float $discount
     */
    public function __construct(int $quantity, float $discount)
    {
        $this->quantity = $quantity;
        $this->discount = $discount;
    }

    /**
     * @param int $numberOfPurchasedItems
     *
     * @return float
     */
    public function getDiscount($numberOfPurchasedItems): float
    {
        if ($numberOfPurchasedItems >= $this->quantity){
            return (int)($numberOfPurchasedItems / $this->quantity) * $this->discount;
        }
        return 0.0;
    }
}