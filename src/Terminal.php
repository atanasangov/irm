<?php

namespace IRM;

/**
 * Class Terminal
 * @package IRM
 *
 * @TODO Known items validation
 * @TODO Validations for discounts
 */
class Terminal
{
    private $sum = 0.0;
    /** @var VolumeDiscount[] */
    private $discounts = [];
    /** @var array */
    private $scanned = [];
    /** @var array */
    private $prices = [];

    /**
     * Gets the total amount of currently scanned items
     *
     * @return float
     */
    public function getTotal(): float
    {
        return $this->sum - $this->applyDiscounts();
    }

    /**
     * @Disclaimer: This method overrides any previous prices for the same product
     *
     * @param string $item
     * @param float  $price
     */
    public function setPricing(string $item, float $price): void
    {
        $this->prices[$item] = $price;
    }

    /**
     * Scans an item
     * @param string $itemName
     */
    public function scanItem(string $itemName)
    {
        $this->scanned[] = $itemName;
        $this->sum += $this->prices[$itemName];
    }

    /**
     * @Disclaimer: This method overrides any previous discounts for the same product
     * @param string $itemName
     * @param DiscountInterface $discount
     */
    public function setDiscount(string $itemName, DiscountInterface $discount): void
    {
        $this->discounts[$itemName] = $discount;
    }

    /**
     * @return float|int
     */
    private function applyDiscounts()
    {
        $purchasedQuantities = array_count_values($this->scanned);
        $discountAmount = 0;

        foreach ($purchasedQuantities as $itemName => $quantityPurchased) {
            $discountAmount += !empty($this->discounts[$itemName]) ? $this->discounts[$itemName]->getDiscount($quantityPurchased) : 0.0;
        }
        return $discountAmount;
    }
}