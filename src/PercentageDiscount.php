<?php

namespace IRM;


class PercentageDiscount implements DiscountInterface
{
    /** @var float */
    protected $percents;
    /** @var float */
    protected $itemPrice;

    /**
     * PercentageDiscount constructor.
     *
     * @param float $percents
     * @param float $itemPrice
     */
    public function __construct(float $percents, float $itemPrice)
    {
        $this->percents = $percents;
        $this->itemPrice = $itemPrice;
    }

    /**
     * @inheritDoc
     */
    public function getDiscount($purchasedQuantity): float
    {
        return (($this->percents / 100) * $this->itemPrice) * $purchasedQuantity;
    }
}