<?php

namespace IRM;


interface DiscountInterface
{
    /**
     * Gets the discount for the purchased items. They should be of the same type.
     *
     * @param int $purchasedQuantity
     *
     * @return float
     */
    public function getDiscount($purchasedQuantity): float;
}