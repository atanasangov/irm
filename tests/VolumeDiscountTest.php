<?php

use IRM\VolumeDiscount;
use PHPUnit\Framework\TestCase;

class VolumeDiscountTest extends TestCase
{
    public function testNoVolumeDiscount()
    {
        $vd = new VolumeDiscount(2, 3);
        $this->assertSame(0.0, $vd->getDiscount(1));
    }

    public function testGiveDiscountForABulkOf2ItemsWhen3ArePurchased()
    {
        $vd = new VolumeDiscount(2, 7);
        $this->assertSame(7.0, $vd->getDiscount(3));
    }

    public function testGiveDiscountForABulkOf2ItemsWhen2ArePurchased()
    {
        $vd = new VolumeDiscount(2, 7);
        $this->assertSame(7.0, $vd->getDiscount(2));
    }

    public function testGiveDiscountForTwoBulksOf2ItemsWhen5ArePurchased()
    {
        $vd = new VolumeDiscount(2, 7);
        $this->assertSame(14.0, $vd->getDiscount(5));
    }
}