<?php

use IRM\PercentageDiscount;
use PHPUnit\Framework\TestCase;


class PercentageDiscountTest extends TestCase
{
    public function testGetPercentageDiscountOnOneItem()
    {
        $pd = new PercentageDiscount(10, 30);
        $this->assertSame(3.0, $pd->getDiscount(1));
    }

    public function testGetPercentageDiscountOnTwoItems()
    {
        $pd = new PercentageDiscount(10, 30);
        $this->assertSame(6.0, $pd->getDiscount(2));
    }
}