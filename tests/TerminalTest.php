<?php

use IRM\PercentageDiscount;
use IRM\Terminal;
use IRM\VolumeDiscount;
use PHPUnit\Framework\TestCase;

class TerminalTest extends TestCase
{
    /** @var Terminal */
    protected $terminal;

    protected function setUp(): void
    {
        $this->terminal = new Terminal();
    }

    public function testBrandNewTotalIsZero()
    {
        $this->assertSame(0.0, $this->terminal->getTotal());
    }

    public function testOneItemReturnsTheItemsPrice()
    {
        $itemPrice = 1.0;
        $item = 'itemName';
        $this->terminal->setPricing($item, $itemPrice);
        $this->terminal->scanItem($item);
        $this->assertSame($itemPrice, $this->terminal->getTotal());
    }

    public function testTwoItemsOfTheSameTypeReturnTwiceThePrice()
    {
        $itemPrice = 1.0;
        $item = 'itemName';
        $this->terminal->setPricing($item, $itemPrice);
        $this->terminal->scanItem($item);
        $this->terminal->scanItem($item);
        $this->assertSame(2 * $itemPrice, $this->terminal->getTotal());
    }

    public function testThreeDifferentProductsSumUpToACorrectTotal()
    {
        $firstItemPrice = 0.25;
        $firstItemName = 'first';
        $this->terminal->setPricing($firstItemName, $firstItemPrice);
        $secondItemPrice = 12.00;
        $secondItemName = 'second';
        $this->terminal->setPricing($secondItemName, $secondItemPrice);
        $thirdItemPrice = 2.00;
        $thirdItemName = 'third';
        $this->terminal->setPricing($thirdItemName, $thirdItemPrice);
        $this->terminal->scanItem($firstItemName);
        $this->terminal->scanItem($secondItemName);
        $this->terminal->scanItem($thirdItemName);

        $this->assertEquals($firstItemPrice + $secondItemPrice + $thirdItemPrice, $this->terminal->getTotal());
    }

    public function testVolumeDiscountForOneProduct6EachAnd4ForTwo()
    {
        $itemPrice = 6.0;
        $itemName = 'first';
        $volumeDiscount = 2.0;
        $this->terminal->setPricing($itemName, $itemPrice);
        $this->terminal->setDiscount($itemName, new VolumeDiscount(2, $volumeDiscount));
        $this->terminal->scanItem($itemName);
        $this->assertSame($itemPrice, $this->terminal->getTotal());
        $this->terminal->scanItem($itemName);
        $this->assertSame($itemPrice + $itemPrice - $volumeDiscount, $this->terminal->getTotal());
    }

    public function testFirstExample()
    {
        $this->setPricesAndDiscountsFromTheTask();

        $this->scan('ZA', 'YB', 'FC', 'GD', 'ZA', 'YB', 'ZA', 'ZA');

        $this->assertSame(32.40, $this->terminal->getTotal());
    }

    public function testSecondExample()
    {
        $this->setPricesAndDiscountsFromTheTask();

        $this->scan('FC', 'FC', 'FC', 'FC', 'FC', 'FC', 'FC');

        $this->assertSame(7.25, $this->terminal->getTotal());
    }

    public function testThirdExample()
    {
        $this->setPricesAndDiscountsFromTheTask();

        $this->scan('ZA', 'YB', 'FC', 'GD');

        $this->assertSame(15.40, $this->terminal->getTotal());
    }

    public function testPercentageDiscount()
    {
        $this->terminal->setPricing('XX', 25);
        $this->terminal->setDiscount('XX', new PercentageDiscount(20, 25));
        $this->scan('XX');
        $this->assertSame(20.0, $this->terminal->getTotal());
    }

    private function scan(...$items)
    {
        foreach($items as $item) {
            $this->terminal->scanItem($item);
        }
    }

    private function setPricesAndDiscountsFromTheTask(): void
    {
        $this->terminal->setPricing('ZA', 2);
        $this->terminal->setDiscount('ZA', new VolumeDiscount(4, 1));
        $this->terminal->setPricing('YB', 12);
        $this->terminal->setPricing('FC', 1.25);
        $this->terminal->setDiscount('FC', new VolumeDiscount(6, 1.5));
        $this->terminal->setPricing('GD', 0.15);
    }
}